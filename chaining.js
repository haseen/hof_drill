const newData = require("./p3.js");

const output = newData.reduce((result, curr) => {
  const cvv = Math.ceil(Math.random() * 1000);
  const currentDate = new Date();
  const date = new Date(curr.issue_date);
  const validity = date.getTime() > currentDate.getTime() ? "Valid" : "Invalid";
  const newValue = { ...curr, cvv, validity };
  result.push(newValue);
  return result;
}, []);

console.log(output);

  